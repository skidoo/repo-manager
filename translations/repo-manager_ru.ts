<?xml version="1.0" ?><!DOCTYPE TS><TS language="ru" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="ui_mainwindow.h" line="332"/>
        <source>Repo Manager</source>
        <translation>Менеджер репозиториев</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <location filename="mainwindow.cpp" line="487"/>
        <location filename="ui_mainwindow.h" line="333"/>
        <source>Select the APT repository that you want to use:</source>
        <translation>Выберите APT репозиторий, который Вы хотите использовать:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="59"/>
        <location filename="ui_mainwindow.h" line="337"/>
        <source>antiX repos</source>
        <translation>antiX репозитории</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <location filename="ui_mainwindow.h" line="334"/>
        <source>Select fastest antiX repo for me</source>
        <translation>Выберите самый быстрый для себя antiX репозиторий</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <location filename="ui_mainwindow.h" line="336"/>
        <source>search</source>
        <translation>поиск</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <location filename="ui_mainwindow.h" line="339"/>
        <source>Debian repos</source>
        <translation>Debian репозитории</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="184"/>
        <location filename="ui_mainwindow.h" line="338"/>
        <source>Select fastest Debian repo for me</source>
        <translation>Выберите самый быстрый для себя Debian репозиторий</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <location filename="ui_mainwindow.h" line="341"/>
        <source>Individual sources</source>
        <translation>Частные источники</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <location filename="ui_mainwindow.h" line="340"/>
        <source>Restore original APT sources</source>
        <translation>Восстановить изначальные источники APT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="343"/>
        <source>Display help </source>
        <translation>Показать справку</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <location filename="ui_mainwindow.h" line="345"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <location filename="ui_mainwindow.h" line="346"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <location filename="ui_mainwindow.h" line="348"/>
        <source>About this application</source>
        <translation>Об этом приложении</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="350"/>
        <source>About...</source>
        <translation>O...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="351"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <location filename="ui_mainwindow.h" line="353"/>
        <source>Quit application</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="422"/>
        <location filename="ui_mainwindow.h" line="355"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="429"/>
        <location filename="ui_mainwindow.h" line="356"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <location filename="ui_mainwindow.h" line="357"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Please wait...</source>
        <translation>Пожалуйста, ждите...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="128"/>
        <location filename="mainwindow.cpp" line="356"/>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Success</source>
        <translation>Успешно</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <location filename="mainwindow.cpp" line="357"/>
        <location filename="mainwindow.cpp" line="610"/>
        <source>Your new selection will take effect the next time sources are updated.</source>
        <translation>Ваш новый выбор вступит в силу при следующем обновлении.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Lists</source>
        <translation>Списки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Sources (checked sources are enabled)</source>
        <translation>Источники (отмеченные источники включены)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <location filename="mainwindow.cpp" line="537"/>
        <location filename="mainwindow.cpp" line="548"/>
        <location filename="mainwindow.cpp" line="566"/>
        <location filename="mainwindow.cpp" line="594"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>Could not change the repo.</source>
        <translation>Не удалось изменить репозиторий.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="402"/>
        <source>About %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="403"/>
        <source>Version: </source>
        <translation>Версия: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="404"/>
        <source>Program for choosing the default APT repository</source>
        <translation>Программа для выбора APT репозитория по умолчанию</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="406"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="422"/>
        <source>%1 Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>You have selected MX Test Repo. It&apos;s not recommended to leave it enabled or to upgrade all the packages from it.</source>
        <translation>Вы выбрали MX Тестовый репозиторий. Не рекомендуется оставлять его активированным или обновлять из него все пакеты.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="432"/>
        <source>A safer option is to install packages individually with MX Package Installer.</source>
        <translation>Безопасной является возможность устанавливать отдельные пакеты через MX Установщик Пакетов.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <source>Select the APT repository and sources that you want to use:</source>
        <translation>Выберите APT репозиторий и источники, которые Вы хотите использовать:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="538"/>
        <source>netselect-apt could not detect fastest repo.</source>
        <translation>netselect-apt не смог обнаружить самый быстрый репозиторий.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="549"/>
        <location filename="mainwindow.cpp" line="567"/>
        <source>Could not detect fastest repo.</source>
        <translation>Не удалось определить самый быстрый репозиторий.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="595"/>
        <source>Could not download original APT files.</source>
        <translation>Не удалось загрузить оригинальные файлы APT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Original APT sources have been restored to the release status. User added source files in /etc/apt/sources.list.d/ have not been touched.</source>
        <translation>Оригинальные источники APT были восстановлены до состояния релиза. Добавленные пользователем файлы источников в /etc/apt/sources.list.d/ не были затронуты.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Список изменений</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>You must run this program as root.</source>
        <translation>Вы должны запустить программу от имени суперпользователя.</translation>
    </message>
</context>
</TS>
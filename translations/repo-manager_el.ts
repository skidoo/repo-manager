<?xml version="1.0" ?><!DOCTYPE TS><TS language="el" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="ui_mainwindow.h" line="332"/>
        <source>Repo Manager</source>
        <translation>Repo Manager</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <location filename="mainwindow.cpp" line="487"/>
        <location filename="ui_mainwindow.h" line="333"/>
        <source>Select the APT repository that you want to use:</source>
        <translation>Επιλέξτε την αποθήκη του APT που θέλετε να χρησιμοποιήσετε.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="59"/>
        <location filename="ui_mainwindow.h" line="337"/>
        <source>antiX repos</source>
        <translation>Αποθήκες του antiΧ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <location filename="ui_mainwindow.h" line="334"/>
        <source>Select fastest antiX repo for me</source>
        <translation>Επιλέξτε την ταχύτερη αποθήκη του antiΧ για μένα</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <location filename="ui_mainwindow.h" line="336"/>
        <source>search</source>
        <translation>aναζήτηση </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <location filename="ui_mainwindow.h" line="339"/>
        <source>Debian repos</source>
        <translation>Debian repos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="184"/>
        <location filename="ui_mainwindow.h" line="338"/>
        <source>Select fastest Debian repo for me</source>
        <translation>Επιλέξτε την ταχύτερη αποθήκη του Debian για μένα
</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <location filename="ui_mainwindow.h" line="341"/>
        <source>Individual sources</source>
        <translation>Μεμονωμένες πηγές</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <location filename="ui_mainwindow.h" line="340"/>
        <source>Restore original APT sources</source>
        <translation>Επαναφορά αρχικών πηγών APT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="343"/>
        <source>Display help </source>
        <translation>Δείτε Βοήθεια</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <location filename="ui_mainwindow.h" line="345"/>
        <source>Help</source>
        <translation>Βοήθεια </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <location filename="ui_mainwindow.h" line="346"/>
        <source>Alt+H</source>
        <translation>Alt+H </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <location filename="ui_mainwindow.h" line="348"/>
        <source>About this application</source>
        <translation>Περί εφαρμογής.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="350"/>
        <source>About...</source>
        <translation>Περί</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="351"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <location filename="ui_mainwindow.h" line="353"/>
        <source>Quit application</source>
        <translation>Κλείστε την εφαρμογή </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="422"/>
        <location filename="ui_mainwindow.h" line="355"/>
        <source>Close</source>
        <translation>Κλείσε</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="429"/>
        <location filename="ui_mainwindow.h" line="356"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <location filename="ui_mainwindow.h" line="357"/>
        <source>Apply</source>
        <translation>Εφαρμογή</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Please wait...</source>
        <translation>Παρακαλώ περιμένετε...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="128"/>
        <location filename="mainwindow.cpp" line="356"/>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Success</source>
        <translation>Επιτυχία!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <location filename="mainwindow.cpp" line="357"/>
        <location filename="mainwindow.cpp" line="610"/>
        <source>Your new selection will take effect the next time sources are updated.</source>
        <translation>Η νέα επιλογή σας θα τεθεί σε ισχύ την επόμενη φορά που ενημερώνονται οι πηγές.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Lists</source>
        <translation>Κατάλογος</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Sources (checked sources are enabled)</source>
        <translation>Πηγές (ενεργοποιημένες ελεγχόμενες πηγές)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <location filename="mainwindow.cpp" line="537"/>
        <location filename="mainwindow.cpp" line="548"/>
        <location filename="mainwindow.cpp" line="566"/>
        <location filename="mainwindow.cpp" line="594"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>Could not change the repo.</source>
        <translation>Δεν ήταν δυνατή η αλλαγή.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="402"/>
        <source>About %1</source>
        <translation>Περί %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="403"/>
        <source>Version: </source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="404"/>
        <source>Program for choosing the default APT repository</source>
        <translation>Πρόγραμμα για την επιλογή της προεπιλεγμένης αποθήκης του APT</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="406"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c)  MX Linux </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 License</source>
        <translation>%1 Άδεια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="422"/>
        <source>%1 Help</source>
        <translation>%1 Βοήθεια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <source>Warning</source>
        <translation>Προειδοποίηση </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>You have selected MX Test Repo. It&apos;s not recommended to leave it enabled or to upgrade all the packages from it.</source>
        <translation>Έχετε επιλέξει το MX Test Repo. Δεν συνιστάται η ενεργοποίησή του ή η αναβάθμιση όλων των πακέτων από αυτό.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="432"/>
        <source>A safer option is to install packages individually with MX Package Installer.</source>
        <translation>Μια ασφαλέστερη επιλογή είναι να εγκαταστήσετε τα πακέτα μεμονωμένα με το MX Package Installer.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <source>Select the APT repository and sources that you want to use:</source>
        <translation>Επιλέξτε το APT repository και τις πηγές για χρήση:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="538"/>
        <source>netselect-apt could not detect fastest repo.</source>
        <translation>Το netselect-apt δεν μπόρεσε να ανιχνεύσει την ταχύτερη αποθήκη</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="549"/>
        <location filename="mainwindow.cpp" line="567"/>
        <source>Could not detect fastest repo.</source>
        <translation>Δεν μπόρεσε να ανιχνεύσει την ταχύτερη αποθήκη</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="595"/>
        <source>Could not download original APT files.</source>
        <translation>Δεν ήταν δυνατή η λήψη πρωτοτύπων αρχείων APT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Original APT sources have been restored to the release status. User added source files in /etc/apt/sources.list.d/ have not been touched.</source>
        <translation>Οι αρχικές πηγές APT έχουν αποκατασταθεί. Τα αρχεία των χρηστών που προστέθηκαν στο /etc/apt/sources.list.d/ δεν έχουν τροποποιηθεί.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Άδεια</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Καταγραφή αλλαγών</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>You must run this program as root.</source>
        <translation>Πρέπει να τρέξετε αυτή την εφαρμογή ως Root.</translation>
    </message>
</context>
</TS>
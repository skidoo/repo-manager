#include "about.h"
#include "cmd.h"
#include "version.h"

#include <QApplication>
#include <QFileInfo>
#include <QMessageBox>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>

// display doc as nomal user when run as root
void displayDoc(QString url, QString title, bool runned_as_root)
{
    if (system("command -v antix-viewer >/dev/null") == 0) {
        system("antix-viewer " + url.toUtf8() + " \"" + title.toUtf8() + "\"&");
    } else {
        if (!runned_as_root) {
            system("xdg-open " + url.toUtf8());
        } else {
            Cmd cmd;
            QString user = cmd.getCmdOut("logname", true);
            system("su " + user.toUtf8() + " -c \"env XDG_RUNTIME_DIR=/run/user/$(id -u " +
                   user.toUtf8() + ") xdg-open " + url.toUtf8() + "\"&");
        }
    }
}

void displayAboutMsgBox(QString title, QString message, QString licence_url, QString license_title, bool runned_as_root)
{
    QMessageBox msgBox(QMessageBox::NoIcon, title, message);
    QPushButton *btnLicense = msgBox.addButton(QApplication::tr("License"), QMessageBox::HelpRole);
    QPushButton *btnCancel = msgBox.addButton(QApplication::tr("Cancel"), QMessageBox::NoRole);
    btnCancel->setIcon(QIcon::fromTheme("window-close"));

    msgBox.exec();

    if (msgBox.clickedButton() == btnLicense) {
        displayDoc(licence_url, license_title, runned_as_root);
    }
}
